//Define an angular module for our app
var app = angular.module('myMovies', ['ui.bootstrap']);

app.controller('searchMoviesController', function ($scope, $http) {
    //getCountries(); // Load all countries with capitals
    $scope.movieList = false;
    $scope.noMovieFound = false;
    $scope.getCountries = function () {
        $scope.movies = {};
        $scope.isDetails = false;
        $http.get("http://www.omdbapi.com/?t=" + $scope.selectedMovie + "&y=" + $scope.selectedYear + "&plot=full&r=json").success(function (data) {
            if (data.Error) {

                $scope.movieList = false;
                $scope.noMovieFound = true;
            } else {
                $scope.movies = data;

                $scope.noMovieFound = false;
                $scope.movieList = true;
            }
        });
    };

    $scope.selectFunction = function () {
        $scope.isDetails = true;
    }
});